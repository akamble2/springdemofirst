package com.agsft.Main;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.agsft.Componant.College;
import com.agsft.Componant.Student;
import com.agsft.Componant.Teacher;

import sun.misc.Resource;

public class Demo {
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		ApplicationContext applicationcontext = new ClassPathXmlApplicationContext("Spring.xml");
		
		College clg=(College) applicationcontext.getBean("college");
		
		Student student =(Student) applicationcontext.getBean("student");
		
		Teacher teacher = (Teacher) applicationcontext.getBean("teacher");
		
		System.out.println("Enter student details 1:Name 2:Age 3:class");
		student.setName(scanner.nextLine());
		student.setAge(scanner.nextInt());
		scanner.nextLine();
		student.setStandard(scanner.nextLine());
		
		System.out.println("Enter Teacher details 1:Name 2:Address 3: Education");
		
		teacher.setName(scanner.nextLine());
		teacher.setAddress(scanner.nextLine());
		teacher.setEducation(scanner.nextLine());
		
		clg.setStudent(student);
		clg.setTeacher(teacher);
		
		System.out.println("College details  "+clg);
		
		
		
		
	}

}
