package com.agsft.Componant;

public class College {
	
	private Teacher teacher;
	private Student student;
	public Teacher getTeacher() {
		return teacher;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	@Override
	public String toString() {
		return "College [teacher=" + teacher + ", student=" + student + "]";
	}
	
	

}
