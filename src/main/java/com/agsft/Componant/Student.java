package com.agsft.Componant;

public class Student {
	
	private String name;
	private int age;
	private String standard;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", standard=" + standard + "]";
	}
	

}
