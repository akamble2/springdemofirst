package com.agsft.Componant;

public class Teacher {
		
	private String name;
	private String Address;
	private String Education;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getEducation() {
		return Education;
	}
	public void setEducation(String education) {
		Education = education;
	}
	@Override
	public String toString() {
		return "Teacher [name=" + name + ", Address=" + Address + ", Education=" + Education + "]";
	}
	
	
}
